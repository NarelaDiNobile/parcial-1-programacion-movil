﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma : MonoBehaviour
{
    float ypos; // Almacena la posición Y inicial de la plataforma
    Generador _Generator; // Referencia al componente Generador

    // Se ejecuta al inicio
    void Start()
    {
        ypos = transform.position.y; // Almacenar la posición Y inicial
        _Generator = GameObject.Find("Plataformas Generador").GetComponent<Generador>(); // Obtener referencia al generador de plataformas
    }

    // Se ejecuta una vez por frame
    void Update()
    {
        if (transform.position.y < ypos - 10f) // Si la plataforma descendio 10 unidades por debajo de su posición inicial
        {
            _Generator.GenerateTiles(); // Generar una nueva plataforma
            Destroy(this.gameObject); // Destruir la plataforma actual
        }
    }
}

