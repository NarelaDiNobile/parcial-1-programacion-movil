﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Generador : MonoBehaviour
{
    public GameObject PlataformaPrefab; // Prefab de la plataforma

    private float xDiff = 1.1f; // Distancia en X entre plataformas
    private float yDiffSmall = 0.4f; // Diferencia en Y para una plataforma corta
    private float yDiffBig = 1.35f; // Diferencia en Y para una plataforma larga

    private float xpos = -2.5f; // Posición inicial en X
    private float ypos = -4.5f; // Posición inicial en Y

    private string smallTag = "smallTile"; // Tag para plataformas cortas
    private string bigTag = "bigTile"; // Tag para plataformas largas

    private List<GameObject> plataformas = new List<GameObject>(); // Lista de plataformas generadas

    // Se ejecuta al inicio
    void Start()
    {
        for (int i = 0; i < 10; ++i) // Generar 10 plataformas iniciales
        {
            GenerateTiles();
        }
    }

    // Generar una plataforma basada en un valor aleatorio
    public void GenerateTiles()
    {
        int random = Random.Range(0, 5); // Genera un número aleatorio entre 0 y 4
        if (random <= 2)
        {
            GenerateSmallTile(); // Generar una plataforma corta
        }
        else
        {
            GenerateBigTile(); // Generar una plataforma larga
        }
    }

    // Genera una plataforma pequeña
    void GenerateSmallTile()
    {
        xpos += xDiff; // Incrementar la posición en X
        ypos += yDiffSmall; // Incrementar la posición en Y

        GameObject platform = Instantiate(PlataformaPrefab, new Vector3(xpos, ypos, 0), PlataformaPrefab.transform.rotation); // Instanciar la plataforma
        platform.tag = smallTag; // Asignar el tag de plataforma pequeña
        plataformas.Add(platform); // Añadir la plataforma a la lista
    }

    // Genera una plataforma grande
    void GenerateBigTile()
    {
        xpos += xDiff; // Incrementar la posición en X
        ypos += yDiffBig; // Incrementar la posición en Y

        GameObject platform = Instantiate(PlataformaPrefab, new Vector3(xpos, ypos, 0), PlataformaPrefab.transform.rotation); // Instanciar la plataforma
        platform.tag = bigTag; // Asignar el tag de plataforma grande
        plataformas.Add(platform); // Añadir la plataforma a la lista
    }

    // Se ejecuta una vez por frame
    void Update()
    {
        // Verificar si alguna plataforma se sale de la pantalla
        List<GameObject> platformsToRemove = new List<GameObject>();

        foreach (var platform in plataformas)
        {
            if (platform.transform.position.y < -10) //  -10 es el límite inferior
            {
                platformsToRemove.Add(platform); // Añadir la plataforma a la lista de eliminación
            }
        }

        // Eliminar las plataformas que se salieron de la pantalla
        foreach (var platform in platformsToRemove)
        {
            plataformas.Remove(platform); // acar la plataforma de la lista
            Destroy(platform); // Destruir la plataforma
            GenerateTiles(); // Generar una nueva plataforma para reemplazar la que se destruyó
        }
    }
}
