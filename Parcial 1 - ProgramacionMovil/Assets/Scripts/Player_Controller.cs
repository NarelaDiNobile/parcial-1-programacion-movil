using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Player_Controller : MonoBehaviour
{
    private Rigidbody2D rb; // Referencia al componente Rigidbody2D
    public float fallMultiplier = 4.5f; // Multiplicador de ca�da para aumentar la gravedad al caer
    private bool Grounded = true; // Indica si el jugador est� en el suelo
    private Animator anim; // Referencia al componente Animator
    public GameObject polvoParticula; // Part�cula de polvo
    private bool firstJump = true; // Indica si es el primer salto
    private float prevYpos = -1000; // Almacena la posici�n Y anterior del jugador
    public bool isDead = false; // Indica si el jugador est� muerto
    public GameObject GameOverScreen; // Pantalla de Game Over
    public GameObject jumpButtons; // Botones de salto
    Color[] standColor = new Color[] { Color.red, Color.green, Color.blue, Color.yellow }; // Colores de las plataformas
    int colorIndex = 0; // �ndice de color actual
    int Achievement = 30; // Puntuaci�n para el logro

    private AudioSource audi; // Referencia al componente AudioSource

    // Textos de la puntuaci�n y contador de monedas
    public TextMeshProUGUI scoreText;
    public TextMeshProUGUI coinCounterText;
    private int coinCount = 0; // Contador de monedas

    private char lastJump = 'N'; // Tipo de �ltimo salto ('N' para ninguno)

    private bool vibrationEnabled = true; // Indica si la vibraci�n est� habilitada

    void Start()
    {
        audi = GetComponent<AudioSource>(); // Obtener el componente AudioSource
        rb = GetComponent<Rigidbody2D>(); // Obtener el componente Rigidbody2D
        anim = GetComponent<Animator>(); // Obtener el componente Animator

        ShowJumpButtons(); // Mostrar los botones de salto
        UpdateCoinCounter(); // Actualizar el contador de monedas
    }

    void FixedUpdate()
    {
        if (rb.velocity.y < 0) // Si el jugador est� cayendo
        {
            rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime; // Aumentar la gravedad
        }

        if (transform.position.y + 5f < prevYpos) // Si el jugador cay� demasiado
        {
            if (!isDead)
                Death(); // Activar la l�gica de muerte
        }
    }

    public void ShortJump()
    {
        Jump(true); // Realizar un salto corto
    }

    public void LongJump()
    {
        Jump(false); // Realizar un salto largo
    }

    private void Jump(bool smalljump)
    {
        firstJump = false;
        if (!Grounded) // Si no est� en el suelo, no saltar
            return;

        Grounded = false; // Indica que ya no est� en el suelo
        if (smalljump)
        {
            lastJump = 'S'; // Marcar el �ltimo salto como corto
            anim.SetTrigger("jump"); // Activar la animaci�n de salto
            rb.AddForce(new Vector2(9.8f * 12f, 9.8f * 20f)); // Aplicar fuerza para un salto corto
        }
        else
        {
            lastJump = 'B'; // Marcar el �ltimo salto como largo
            anim.SetTrigger("jump"); // Activar la animaci�n de salto
            StartCoroutine(LongJumpCoroutine()); // Iniciar la rutina de salto largo
        }
    }

    IEnumerator LongJumpCoroutine()
    {
        rb.AddForce(new Vector2(0, 9.8f * 29f)); // Aplicar fuerza vertical para un salto largo
        yield return new WaitForSeconds(0.15f); // Esperar 0.15 segundos
        rb.AddForce(new Vector2(9.8f * 12f, 0)); // Aplicar fuerza horizontal para un salto largo
    }

    void OnCollisionEnter2D(Collision2D col)
    {
        rb.velocity = Vector3.zero; // Detener el movimiento

        if (lastJump == 'S' && col.gameObject.tag == "smallTile")
        {
            HandleCorrectJump(col); // Manejar un salto corto correcto
        }
        else if (lastJump == 'B' && col.gameObject.tag == "bigTile")
        {
            HandleCorrectJump(col); // Manejar un salto largo correcto
        }
        else if (lastJump == 'N')
        {
        }
        else
        {
            GameOverScreen.SetActive(true); // Mostrar la pantalla de Game Over
            isDead = true; // Marcar al jugador como muerto
            HideJumpButtons(); // Ocultar los botones de salto
        }

        if (col.gameObject.tag.Contains("Tile"))
        {
            audi.Play(); // Reproducir sonido
            int currentScore;
            if (int.TryParse(scoreText.text, out currentScore))
            {
                currentScore++; // Incrementar la puntuaci�n
                scoreText.text = currentScore.ToString(); // Actualizar el texto de la puntuaci�n
            }

            Renderer cr = col.gameObject.GetComponent<Renderer>();
            cr.material.SetColor("_Color", standColor[colorIndex]); // Cambiar el color de la plataforma

            prevYpos = transform.position.y; // Actualizar la posici�n Y anterior

            GameObject temp = Instantiate(polvoParticula, new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z), polvoParticula.transform.rotation);
            Destroy(temp, 1.5f); // Crear y destruir part�culas de polvo

            Grounded = true; // Marcar al jugador como en el suelo
            transform.position = new Vector3(col.gameObject.transform.position.x - 0.2f, transform.position.y, transform.position.z); // Ajustar la posici�n del jugador

            if (firstJump)
                return;

            StartCoroutine(FallTile(col.gameObject)); // Iniciar la rutina para que la plataforma caiga
        }

        AchievementAchieved(); // Verificar si se logro un objetivo
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Coin")
        {
            coinCount++; // Incrementar el contador de monedas
            UpdateCoinCounter(); // Actualizar el contador de monedas
            Destroy(other.gameObject); // Destruir la moneda

            if (vibrationEnabled)
            {
                Handheld.Vibrate(); // Activar la vibraci�n
            }
        }
    }

    public void ToggleVibration()
    {
        vibrationEnabled = !vibrationEnabled; // Alternar la vibraci�n
    }

    void AchievementAchieved()
    {
        int currentScore;
        if (int.TryParse(scoreText.text, out currentScore))
        {
            if (currentScore == Achievement)
            {
                if (colorIndex >= standColor.Length - 1)
                {
                    colorIndex = 0; // Reiniciar el �ndice de color
                }
                else
                {
                    colorIndex++; // Incrementar el �ndice de color
                }
                Achievement *= 2; // Doblar la puntuaci�n del siguiente logro
            }
        }
    }

    IEnumerator FallTile(GameObject col)
    {
        yield return new WaitForSeconds(1f);
        if (col.gameObject != null && col.GetComponent<Rigidbody2D>() == null)
        {
            col.AddComponent<Rigidbody2D>(); // A�adir Rigidbody2D para que la plataforma caiga
        }
    }

    void Death()
    {
        GameOverScreen.SetActive(true); // Mostrar la pantalla de Game Over
        isDead = true; // Marcar al jugador como muerto
        HideJumpButtons(); // Ocultar los botones de salto
    }

    public void RestartGame()
    {
        isDead = false; // Reiniciar la variable de muerte
        scoreText.text = "0"; // Reiniciar la puntuaci�n
        coinCount = 0; // Reiniciar el contador de monedas
        UpdateCoinCounter(); // Actualizar el contador de monedas

        ShowJumpButtons(); // Mostrar los botones de salto

        GameOverScreen.SetActive(false); // Ocultar la pantalla de Game Over

        transform.position = new Vector3(0f, 0f, 0f); // Reiniciar la posici�n del jugador
    }

    void HideJumpButtons()
    {
        jumpButtons.SetActive(false); // Ocultar los botones de salto
    }

    void ShowJumpButtons()
    {
        jumpButtons.SetActive(true); // Mostrar los botones de salto
    }

    void HandleCorrectJump(Collision2D col)
    {
    }

    void UpdateCoinCounter()
    {
        coinCounterText.text = "" + coinCount.ToString(); // Actualizar el contador de monedas
    }
}
